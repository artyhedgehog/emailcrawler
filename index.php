<?php
/**
 * SearchModule application front controller page.
 *
 * @author Artem Kolomycev <a.kolomycev@timeweb.ru>
 */

require 'lib/timeweb/akolomycev/utils/classautoloader.class.php';

use Timeweb\AKolomycev\utils\ClassAutoloader;
use Timeweb\AKolomycev\SearchModule\controller\Controller;

// Registring autoloader:
$cl = new ClassAutoloader();
$cl->register();

// Run application:
Controller::run();
