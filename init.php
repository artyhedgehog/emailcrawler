<?php
require 'lib/timeweb/akolomycev/utils/classautoloader.class.php';
use Timeweb\AKolomycev\utils\ClassAutoloader;
use Timeweb\AKolomycev\searchmodule\model\data\DataMapper;
use Timeweb\AKolomycev\searchmodule\model\data\Config;

$cl = new ClassAutoloader();
$cl->register();

$db = new DataMapper(Config::DB_DSN, Config::DB_USER, Config::DB_PASS);
$db->initDatabase();
