<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 02.07.14
 * Time: 19:06
 */

namespace Timeweb\AKolomycev\searchmodule\model\parser;


class ImagesFinder extends Finder
{
    const TYPE = 'images';

    const PATTERN = '_(?P<match><img [^>]*src=(?P<src>"[^"\']*"|\'[^\']*\'|[^\\s"\'<>]+)[^>]*>)_i';

    /**
     * Get pattern to search for.
     * Pattern may contain submask named 'match' for having only this submask
     * for match.
     * @return string Pattern string suitable for preg_match_all.
     */
    protected function getPattern()
    {
        return self::PATTERN;
    }

    /**
     * Get type of finder.
     * @return string
     */
    public function getType()
    {
        return self::TYPE;
    }
}