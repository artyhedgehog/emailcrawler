<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 02.07.14
 * Time: 17:12
 */

namespace Timeweb\AKolomycev\searchmodule\model\parser;


class LinksFinder extends Finder
{
    const TYPE = 'links';

    const PATTERN = '_(?P<match><a [^>]*href=(?P<href>"[^"\']*"|\'[^\']*\'|[^\s"\'<>]+)(?P<content>[^>]*>(?:[^<]*<(?:[^/]|/[^a]))*[^<]*)</a>)_i';

    /**
     * Get pattern to search for.
     * @return string Pattern string suitable for preg_match_all.
     */
    protected function getPattern()
    {
        return self::PATTERN;
    }

    /**
     * Get type of finder.
     * @return string
     */
    public function getType()
    {
        return self::TYPE;
    }
}