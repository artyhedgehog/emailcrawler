<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 30.06.14
 * Time: 18:26
 */

namespace Timeweb\AKolomycev\searchmodule\model\parser;

use Timeweb\AKolomycev\searchmodule\model\data\PageContents;
use Timeweb\AKolomycev\utils\Validator;

class PageParser
{
    /**
     * @var PageProvider Provider of HTML page to parse.
     */
    protected $pageProvider;

    /**
     * @var Finder Used finder.
     */
    protected $finder;

    /**
     * @param PageProvider $pageProvider Provider of a page to parse.
     * @param string $searchType Search type name.
     * @param string $searchText Text to find.
     */
    public function __construct(PageProvider $pageProvider, $searchType = null,
                                $searchText = null
    ) {
        $this->pageProvider = $pageProvider;
        if (isset($searchType)) {
            $this->assignFinder($searchType);
            if ($this->finder instanceof TextFinder) {
                /** @noinspection PhpUndefinedMethodInspection */
                $this->finder->setSearchText($searchText);
            }
        }
    }

    /**
     * Choose and set finder according to a given search type.
     * @param string $searchType Search type name.
     * @throws \DomainException if unknown type specified.
     */
    public function assignFinder($searchType)
    {
        Validator::validateArgType($searchType, 'string', 'type', __METHOD__);

        switch ($searchType) {
            case 'links':
                $this->finder = new LinksFinder();
                break;
            case 'images':
                $this->finder = new ImagesFinder();
                break;
            case 'text':
                $this->finder = new TextFinder();
                break;
            default:
                throw new \DomainException("Unknown search type: {$searchType}.");
        }
    }

    /**
     * Perform search in page provided by provider with chosen mechanism.
     * @return array Search result as assoc array with keys:<ul>
     *                  <li>'url' - page URL</li>
     *                  <li>'quantity' - found elements quantity</li>
     *                  <li>'found' - string with all found occurrences,
     *                      separated by newline symbol</li></ul>
     */
    public function parse()    {
        $result = array('url' => $this->pageProvider->getUrl());

        $page = $this->pageProvider->getPage();
        $this->finder->setSubject($page);

        $result[PageContents::URL_NAME] = $this->pageProvider->getUrl();
        $result[PageContents::TYPE_NAME] = $this->finder->getType();

        // Perform search. If any found, get them, set zero values otherwise.
        if ($this->finder->search()) {
            $result[PageContents::QUANTITY_NAME]
                    = $this->finder->getOccurrencesCount();
            $result[PageContents::CONTENTS_NAME]
                    = $this->finder->getOccurrencesString();
        } else {
            $result[PageContents::QUANTITY_NAME] = 0;
            $result[PageContents::CONTENTS_NAME] = '';
        }

        return $result;
    }

    /**
     * @return PageProvider
     */
    public function getPageProvider()
    {
        return $this->pageProvider;
    }

    /**
     * @param PageProvider $pageProvider
     */
    public function setPageProvider($pageProvider)
    {
        $this->pageProvider = $pageProvider;
    }

    /**
     * @return Finder
     */
    public function getFinder()
    {
        return $this->finder;
    }

    /**
     * @param Finder $finder
     */
    public function setFinder($finder)
    {
        $this->finder = $finder;
    }

}