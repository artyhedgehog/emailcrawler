<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 02.07.14
 * Time: 12:27
 */

namespace Timeweb\AKolomycev\searchmodule\model\parser;

use Timeweb\AKolomycev\utils\Validator;

class TextFinder extends Finder
{
    const TYPE = 'text';

    const PATTERN_BASE = '/>(?P<match>[^<>]*%s[^<>]*)</i';

    /**
     * @var string Text to find
     */
    protected $searchText;

    /**
     * @param string $searchText
     */
    public function setSearchText($searchText)
    {
        Validator::validateArgType($searchText, 'string', 'searchText',
                                   __METHOD__);
        if ($searchText == '') {
            throw new \DomainException("No text to find provided.");
        }
        $this->searchText = $searchText;
    }

    /**
     * Perform search for a text given.
     * @param string $text Text to search.
     * @return bool True if any elements found, false otherwise.
     */
    public function search($text = null)
    {
        if (isset($text)) {
            $this->setSearchText($text);
        }
        return parent::search();
    }

    /**
     * Get pattern to search for.
     * @return string Pattern string suitable for preg_match_all.
     * @throws \LogicException if no text yet set for search.
     */
    protected function getPattern()
    {
        if (!isset($this->searchText)) {
            throw new \LogicException("Trying to get text pattern with no text set.");
        }

        return sprintf(self::PATTERN_BASE, $this->searchText);
    }

    /**
     * Get type of finder.
     * @return string
     */
    public function getType()
    {
        return self::TYPE;
    }
}