<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 02.07.14
 * Time: 12:06
 */

namespace Timeweb\AKolomycev\searchmodule\model\parser;

use Timeweb\AKolomycev\utils\Validator;

/**
 * Base for elements finder.
 * @package Timeweb\AKolomycev\searchmodule\model
 */
abstract class Finder
{
    /**
     * @var array Found occurences.
     */
    protected $occurrences;

    /**
     * @var string Subject to search in.
     */
    protected $subject;

    /**
     * Get all found occurrences.
     * @return array
     */
    public function getOccurrences()
    {
        if (is_null($this->occurrences)) {
            $this->search();
        }
        return $this->occurrences;
    }

    /**
     * Get all occurrences as one string glued with newline symbol.
     * @return string Imploded occurrences.
     */
    public function getOccurrencesString()
    {
        return implode(PHP_EOL, $this->getOccurrences());
    }

    /**
     * Count found occurrences.
     * @return int Number of found occurrences.
     */
    public function getOccurrencesCount()
    {
        return count($this->getOccurrences());
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        Validator::validateArgType($subject, 'string', 'haystack', __METHOD__);
        // Removing newline symbols from a subject.
        $this->subject = str_replace(PHP_EOL, ' ', $subject);
    }

    /**
     * Performs search and saving occurrences.
     * @return bool True if some matches found, false otherwise.
     */
    public function search()
    {
        $result = preg_match_all($this->getPattern(), $this->getSubject(),
                                 $matches);

        if ($result === false) {
            // Error occurred.
            throw new \RuntimeException("Error occurred while searching.");
        }

        // Save 'match' submask only if defined.
        if (array_key_exists('match', $matches)) {
            $this->occurrences = $matches['match'];
        } else {
            $this->occurrences = $matches[0];
        }

        return (bool) $result;
    }

    /**
     * Get type of finder.
     * @return string
     */
    abstract public function getType();

    /**
     * Get pattern to search for.
     * Pattern may contain submask named 'match' for having only this submask
     * for match.
     * @return string Pattern string suitable for preg_match_all.
     */
    abstract protected function getPattern();

}
