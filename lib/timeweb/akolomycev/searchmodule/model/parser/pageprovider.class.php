<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 01.07.14
 * Time: 13:33
 */

namespace Timeweb\AKolomycev\searchmodule\model\parser;
use Timeweb\AKolomycev\utils\Validator;

/**
 * Loads remote HTML page via HTTP.
 * @package Timeweb\AKolomycev\searchmodule\model
 */
class PageProvider
{
    /**
     * Pattern to match URL.
     */
    const URL_PATTERN = '_^(?P<protocol>https?://)(?P<domain>[-a-z0-9]+(\\.[-a-z0-9]+)+)(?P<path>(/([-\\w\\.!~*\'\\(\\)]|%\\w{2})*)*)(?P<query>(\\?([-\\w\\.!~*\'\\(\\)]|%\\w{2})+(=([-\\w\\.!~*\'\\(\\)]|%\\w{2})+)?(&([-\\w\\.!~*\'\\(\\)]|%\\w{2})+(=([-\\w\\.!~*\'\\(\\)]|%\\w{2})+)?)*)?)$_';

    /**
     * @var string URL of a page.
     */
    protected $url;

    /**
     * @var string HTML page loaded.
     */
    protected $page;

    /**
     * @param string $url URL of a page to load.
     */
    public function __construct($url)
    {
        $this->setUrl($url);
    }

    /**
     * Get loaded page content.
     *
     * @return string Page content.
     */
    public function getPage()
    {
        if (!isset($this->page)) {
            $this->loadPage();
        }
        return $this->page;
    }

    /**
     * Load remote page with URL set.
     */
    public function loadPage()
    {
        if (!isset($this->url)) {
            throw new \LogicException('Tried to load page with no URL set.');
        }
        $result = @file_get_contents($this->url);
        if ($result === false) {
            throw new \RuntimeException("Could not load HTML page with URL: {$this->url}.");
        }
        $this->page = $result;
    }

    /**
     * Validates, encode if necessary and sets URL.
     *
     * @param string $url URL to set.
     */
    public function setUrl($url)
    {
        Validator::validateArgType($url, 'string', 'url', __METHOD__);

        if ($url != $this->url) {
            if (!preg_match(self::URL_PATTERN, $url, $matches)) {
                throw new \DomainException('Given URL does not fit URL pattern.');
            }
            $this->url = $url;
            // Page loaded is no longer relevant.
            $this->page = null;
        }
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

}