<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 06.07.14
 * Time: 15:47
 */

namespace Timeweb\AKolomycev\searchmodule\model\data;
use Timeweb\AKolomycev\utils\Validator;

/**
 * Representation of contents found on a page.
 * @package Timeweb\AKolomycev\searchmodule\model\data
 */
class PageContents
{
    /**
     * @const string
     */
    const TABLE_NAME = 'pages_contents';

    /**
     * @const string
     */
    const URL_NAME = 'url';

    /**
     * @const string
     */
    const TYPE_NAME = 'type';

    /**
     * @const string
     */
    const QUANTITY_NAME = 'quantity';

    /**
     * @const string
     */
    const CONTENTS_NAME = 'contents';

    /**
     * @var array Stored values.
     */
    protected $data;

    /**
     * @return string Database table name.
     */
    public static function getTableName()
    {
        return self::TABLE_NAME;
    }

    /**
     * @return array Array listed used types names.
     */
    public static function getSearchTypes()
    {
        return array('images', 'links', 'text');
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        Validator::validateArgType($data[self::URL_NAME], 'string');
        Validator::validateArgType($data[self::TYPE_NAME], 'string');
        Validator::validateArgType($data[self::QUANTITY_NAME], 'integer');
        Validator::validateArgType($data[self::CONTENTS_NAME], 'string');

        $this->data[self::URL_NAME] = $data[self::URL_NAME];
        $this->data[self::TYPE_NAME] = $data[self::TYPE_NAME];
        $this->data[self::QUANTITY_NAME] = $data[self::QUANTITY_NAME];
        $this->data[self::CONTENTS_NAME] = $data[self::CONTENTS_NAME];
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->data[self::URL_NAME];
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->data[self::TYPE_NAME];
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->data[self::QUANTITY_NAME];
    }

    /**
     * @return string
     */
    public function getContents()
    {
        return $this->data[self::CONTENTS_NAME];
    }

}