<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 06.07.14
 * Time: 15:03
 */

namespace Timeweb\AKolomycev\searchmodule\model\data;

use Timeweb\AKolomycev\utils\Validator;

class DataMapper extends \PDO
{
    // SQL templates.
    const SQL_CREATE_TYPES = 'CREATE TABLE content_types (id SMALLINT PRIMARY KEY AUTO_INCREMENT, type VARCHAR(15) UNIQUE)';
    const SQL_CREATE_PAGES = 'CREATE TABLE pages_contents (url VARCHAR(255), type_id SMALLINT, quantity INT, contents TEXT, PRIMARY KEY (url, type_id), FOREIGN KEY (type_id) REFERENCES content_types(id))';
    const SQL_ADD_TYPE = 'INSERT INTO content_types (type) VALUES (:type)';
    const SQL_WRITE = 'INSERT INTO pages_contents (url, type_id, quantity, contents) VALUES (:url, (SELECT id FROM content_types WHERE type = :type LIMIT 1), :quantity, :contents) ON DUPLICATE KEY UPDATE quantity = :quantity, contents = :contents';
    const SQL_READ_ALL = 'SELECT url, type, quantity, contents FROM pages_contents LEFT JOIN content_types ON id = type_id';

    /**
     * @var array Prepared SQL statements.
     * @use \PDOStatement
     */
    protected $statements;

    /**
     * @param string $dsn
     * @param string $username
     * @param string $password
     * @param array $options
     */
    public function __construct($dsn, $username, $password, $options = array(
            \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    )) {
        Validator::validateArgType($dsn, 'string', 'dsn', __METHOD__);
        Validator::validateArgType($username, 'string', 'username', __METHOD__);
        Validator::validateArgType($password, 'string', 'password', __METHOD__);
        Validator::validateArgType($options, 'array', 'options', __METHOD__);
        $this->statements = array();
        parent::__construct($dsn, $username, $password, $options);
        $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Writes an object to a database.
     * @param PageContents $contents Page found contents to write.
     */
    public function write(PageContents $contents)
    {
        $sth = $this->getStatement(self::SQL_WRITE);

        $data = $contents->getData();

        $sth->bindValue(':url', $data[PageContents::URL_NAME]);
        $sth->bindValue(':type', $data[PageContents::TYPE_NAME]);
        $sth->bindValue(':quantity', $data[PageContents::QUANTITY_NAME]);
        $sth->bindValue(':contents', $data[PageContents::CONTENTS_NAME]);

        $sth->execute();
        $sth->closeCursor();
    }

    /**
     * Reads all data from database and create related objects.
     * @return array Array of @see PageContents objects.
     */
    public function readAll()
    {
        $sth = $this->getStatement(self::SQL_READ_ALL);

        $sth->bindColumn(PageContents::URL_NAME, $url);
        $sth->bindColumn(PageContents::TYPE_NAME, $type);
        $sth->bindColumn(PageContents::QUANTITY_NAME, $quantity);
        $sth->bindColumn(PageContents::CONTENTS_NAME, $contents);

        $sth->execute();

        $pages = array();
        while ($sth->fetch(\PDO::FETCH_BOUND)) {
            $data = array(PageContents::URL_NAME => $url,
                          PageContents::TYPE_NAME => $type,
                          PageContents::QUANTITY_NAME => (int) $quantity,
                          PageContents::CONTENTS_NAME => $contents);
            $page = new PageContents();
            $page->setData($data);
            $pages[] = $page;
        }
        $sth->closeCursor();
        return $pages;
    }

    /**
     * Initialise database.
     */
    public function initDatabase()
    {
        $sth = $this->getStatement(self::SQL_CREATE_TYPES);
        $sth->execute();
        $sth->closeCursor();

        $sth = $this->getStatement(self::SQL_ADD_TYPE);
        foreach (PageContents::getSearchTypes() as $type) {
            $sth->bindValue(':type', $type);
            $sth->execute();
        }
        $sth->closeCursor();

        $sth = $this->getStatement(self::SQL_CREATE_PAGES);
        $sth->execute();
        $sth->closeCursor();
    }

    /**
     * Get prepared statement by SQL template.
     * @param string $sql SQL template.
     * @return \PDOStatement Statement to execute.
     */
    protected function getStatement($sql)
    {
        if (!array_key_exists($sql, $this->statements)) {
            $this->statements[$sql] = $this->prepare($sql);
        }
        return $this->statements[$sql];
    }

}
