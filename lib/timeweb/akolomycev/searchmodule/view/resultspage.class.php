<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 29.06.14
 * Time: 18:32
 */

namespace Timeweb\AKolomycev\searchmodule\view;


use Timeweb\AKolomycev\SearchModule\controller\Controller;

class ResultsPage extends PageView
{
    const TITLE = 'Checked sites';

    /**
     * Provides template name, corresponding to a template filename without
     * extension.
     *
     * @return string Template name.
     */
    public function getTemplateName()
    {
        return 'results';
    }

    public function __construct(Controller $context)
    {
        parent::__construct($context);
        $this->values['pageTitle'] = self::TITLE;
        $this->values['js'][] = 'js/results.js';
    }

    public function display()
    {
        $this->prepareResults();
        parent::display();
    }

    private function prepareResults()
    {
        $results = array();
        foreach ($this->values['pages'] as $page) {
            $result = array('url' => $page->getUrl(),
                            'type' => $page->getType(),
                            'quantity' => $page->getQuantity(),
                            'contents' => htmlentities($page->getContents(),
                                                       ENT_QUOTES, 'UTF-8',
                                                       false));
            $results[] = $result;
        }
        $this->values['pages'] = $results;
    }

}