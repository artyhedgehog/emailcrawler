<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 29.06.14
 * Time: 17:47
 */

namespace Timeweb\AKolomycev\searchmodule\view;

use Timeweb\AKolomycev\SearchModule\controller\Controller;
use Timeweb\AKolomycev\SearchModule\controller\Request;

/**
 * Basic HTML page view.
 *
 * @package Timeweb\AKolomycev\searchmodule\view
 */
abstract class PageView extends View
{
    const TEMPLATES_DIR = 'templates';
    const WIDGETS_SUBDIR = 'widgets';
    const TEMPLATE_EXT = '.phtml';

    public function __construct(Controller $context)
    {
        parent::__construct($context);
        $this->values = array(
            'siteTitle'     =>  'Search module',
            'pageTitle'     =>  '', // -- stub for widgets
            'js'            =>  array('js/jquery-2.1.1.min.js'),
            'css'           =>  '', // -- stub for widgets
            'goToCaption'   =>  'Go to: ',
            'navbarItems'   =>  array(
                FormPage::TITLE => '?' . Request::COMMAND_PROPERTY_NAME
                                 . '=' . Controller::COMMAND_SHOW_FORM,
                ResultsPage::TITLE => '?' . Request::COMMAND_PROPERTY_NAME
                                    . '=' . Controller::COMMAND_SHOW_RESULTS,
            ),
        );
    }

    /**
     * Displays page using template and values set.
     */
    public function display()
    {
        extract($this->values, EXTR_REFS);
        /** @noinspection PhpIncludeInspection */
        include self::TEMPLATES_DIR . DIRECTORY_SEPARATOR
            . $this->getTemplateName() . self::TEMPLATE_EXT;
    }

    /**
     * Provides template name, corresponding to a template filename without
     * extension.
     *
     * @return string Template name.
     */
    abstract public function getTemplateName();

    /**
     * Displays widget from a widget template with given name.
     *
     * @param string $name Name of a widget to display.
     */
    protected function widget($name)
    {
        extract($this->values, EXTR_REFS);
        /** @noinspection PhpIncludeInspection */
        include self::TEMPLATES_DIR . DIRECTORY_SEPARATOR . self::WIDGETS_SUBDIR
            . DIRECTORY_SEPARATOR . $name . self::TEMPLATE_EXT;
    }



}