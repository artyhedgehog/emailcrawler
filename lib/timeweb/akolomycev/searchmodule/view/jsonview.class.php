<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 30.06.14
 * Time: 18:20
 */

namespace Timeweb\AKolomycev\searchmodule\view;

class JsonView extends View
{

    /**
     * Displays page using values set.
     */
    public function display()
    {
        header('Content-Type: application/json');
        echo json_encode($this->values);
    }

}