<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 07.07.14
 * Time: 16:36
 */

namespace Timeweb\AKolomycev\SearchModule\view;

class ErrorPage extends PageView
{

    /**
     * Provides template name, corresponding to a template filename without
     * extension.
     *
     * @return string Template name.
     */
    public function getTemplateName()
    {
        return 'error';
    }
}