<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 27.06.14
 * Time: 17:53
 */

namespace Timeweb\AKolomycev\SearchModule\view;

use Timeweb\AKolomycev\SearchModule\controller\Controller;
use Timeweb\AKolomycev\SearchModule\controller\Request;

class FormPage extends PageView
{
    const TEMPLATE = 'form';
    const TITLE = 'Search form';
    const URL_INPUT_NAME = 'url';
    const TYPE_INPUT_NAME = 'type';
    const TEXT_INPUT_NAME = 'text';

    /**
     * Ought to provide template name.
     *
     * @return string Template name. Corresponds to a template filename
     *                without extension.
     */
    public function getTemplateName()
    {
        return self::TEMPLATE;
    }

    public function __construct(Controller $context)
    {
        parent::__construct($context);
        $this->values['pageTitle'] = self::TITLE;
        $this->values['js'][] = 'js/punycode.min.js';
        $this->values['js'][] = 'js/form.js';
        $this->values['css'] = 'css/basic.css';
        $this->values['formAction'] = '?' . Request::COMMAND_PROPERTY_NAME
                                    . '=' . Controller::COMMAND_PARSE_PAGE;
        $this->values['urlLabel'] = 'Page URL: ';
        $this->values['urlInputName'] = self::URL_INPUT_NAME;
        $this->values['urlPlaceholder'] = 'http://for.example.com/path/to?no=where, etc.';
        $this->values['typeInputName'] = self::TYPE_INPUT_NAME;
        $this->values['linksTypeLabel'] = 'Find links.';
        $this->values['imagesTypeLabel'] = 'Find images.';
        $this->values['textTypeLabel'] = 'Find text.';
        $this->values['searchTextLabel'] = '';
        $this->values['textInputName'] = self::TEXT_INPUT_NAME;
        $this->values['searchTextPlaceholder'] = 'Text to find...';
        $this->values['submitButtonCaption'] = 'Find';
    }

}