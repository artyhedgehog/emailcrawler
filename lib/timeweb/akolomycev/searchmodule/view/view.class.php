<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 27.06.14
 * Time: 16:20
 */

namespace Timeweb\AKolomycev\SearchModule\view;

use Timeweb\AKolomycev\SearchModule\controller\Controller;
use Timeweb\AKolomycev\utils\Validator;

/**
 * Basic view class.
 * @package Timeweb\AKolomycev\SearchModule\view
 */
abstract class View
{

    /**
     * @var Controller View context.
     */
    protected $context;

    /**
     * @var array Values storage to be responded.
     */
    protected $values;

    /**
     * @param Controller $context Context to set.
     */
    public function __construct(Controller $context)
    {
        $this->context = $context;
    }

    /**
     * Get stored value by name.
     * @param string $name Name of a value.
     * @return mixed Value via given key, null if no such key.
     */
    public function getValue($name)
    {
        Validator::validateArgType($name, 'string', 'name', __METHOD__);
        if (array_key_exists($name, $this->values)) {
            return $this->values[$name];
        } else {
            return null;
        }
    }

    /**
     * @param string $name Name to set as key to a value.
     * @param mixed $value Value to save.
     */
    public function setValue($name, $value)
    {
        Validator::validateArgType($name, 'string', 'name', __METHOD__);
        $this->values[$name] = $value;
    }

    /**
     * @param array $values
     */
    public function setValues($values)
    {
        Validator::validateArgType($values, 'array', 'values', __METHOD__);
        $this->values = $values;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Displays page using values set.
     */
    abstract public function display();

}
