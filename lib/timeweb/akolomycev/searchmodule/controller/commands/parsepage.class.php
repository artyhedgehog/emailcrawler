<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 26.06.14
 * Time: 20:48
 */

namespace Timeweb\AKolomycev\SearchModule\controller\commands;

use Timeweb\AKolomycev\searchmodule\model\data\Config;
use Timeweb\AKolomycev\searchmodule\model\data\DataMapper;
use Timeweb\AKolomycev\searchmodule\model\data\PageContents;
use Timeweb\AKolomycev\searchmodule\model\parser\PageParser;
use Timeweb\AKolomycev\searchmodule\model\parser\PageProvider;
use Timeweb\AKolomycev\SearchModule\view\FormPage;
use Timeweb\AKolomycev\searchmodule\view\JsonView;
use Timeweb\AKolomycev\SearchModule\view\View;

class ParsePage extends Command
{
    /**
     * @var View Corresponding view
     */
    private $view;

    /**
     * Method, containing the main execution algorithm.
     */
    protected function doExecute()
    {
        $this->view = new JsonView($this->getContext());
        $request = $this->getContext()->getRequest();
        $url = $request->getProperty(FormPage::URL_INPUT_NAME);
        $type = $request->getProperty(FormPage::TYPE_INPUT_NAME);
        $text = $request->getProperty(FormPage::TEXT_INPUT_NAME);
        $parser = new PageParser(new PageProvider($url), $type, $text);
        $data = $parser->parse();

        $contents = new PageContents();
        $contents->setData($data);
        $db = new DataMapper(Config::DB_DSN, Config::DB_USER,
                             Config::DB_PASS);
        $db->write($contents);

        $msg = "Found {$contents->getQuantity()} items.";
        $result = array('res' => 'ok', 'msg' => $msg, 'data' => $data);
        $this->view->setValues($result);
    }

    /**
     * Things to do on exception caught.
     * Setting fallback view, perform necessary cleanup.
     * @param \Exception $ex Caught exception.
     * @return void
     */
    protected function onException(\Exception $ex)
    {
        $result = array('res' => 'error',
                        'msg' => $ex->getMessage(),
                        'trc' => $ex->getTrace());
        $this->view->setValues($result);
    }


    /**
     * Get corresponding view to display.
     * @return View
     */
    protected function getView()
    {
        return $this->view;
    }

}