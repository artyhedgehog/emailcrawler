<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 26.06.14
 * Time: 20:50
 */

namespace Timeweb\AKolomycev\SearchModule\controller\commands;

use Timeweb\AKolomycev\searchmodule\model\data\Config;
use Timeweb\AKolomycev\searchmodule\model\data\DataMapper;
use Timeweb\AKolomycev\SearchModule\view\ErrorPage;
use Timeweb\AKolomycev\searchmodule\view\ResultsPage;
use Timeweb\AKolomycev\SearchModule\view\View;

class ShowResults extends Command
{
    /**
     * @var View Used view.
     */
    private $view;

    /**
     * Method, containing the main execution algorithm.
     */
    protected function doExecute()
    {
        $db = new DataMapper(Config::DB_DSN, Config::DB_USER, Config::DB_PASS);
        $pages = $db->readAll();
        $this->view = new ResultsPage($this->getContext());
        $this->view->setValue('pages', $pages);
    }

    /**
     * Get corresponding view to display.
     * @return View
     */
    protected function getView()
    {
        return $this->view;
    }

    /**
     * Things to do on exception caught.
     * Setting fallback view, perform necessary cleanup.
     * @param \Exception $ex Caught exception.
     * @return void
     */
    protected function onException(\Exception $ex)
    {
        $this->view = new ErrorPage($this->getContext());
        $this->view->setValue('error', array('msg' => $ex->getMessage(),
                                             'trc' => $ex->getTraceAsString()));
    }
}
