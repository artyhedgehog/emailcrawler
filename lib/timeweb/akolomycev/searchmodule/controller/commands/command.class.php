<?php
namespace Timeweb\AKolomycev\SearchModule\controller\commands;

use Timeweb\AKolomycev\SearchModule\controller\Controller;
use Timeweb\AKolomycev\SearchModule\view\View;

/**
 * Base for command classes.
 * @abstract
 *
 * @author a.kolomycev
 */
abstract class Command
{
    /**
     * @var Controller Execution context object.
     */
    private $context;

    /**
     * Command execution start method.
     *
     * Calls doExecution method. May be needed for additional execution behavior
     * for all Command classes.
     *
     * @return View Corresponding view with necessary data bound.
     * @final
     */
    final public function execute()
    {
        try {
            $this->doExecute();
        } catch (\Exception $ex) {
            $this->onException($ex);
        }
        return $this->getView();
    }

    /**
     * @param Controller $context Context to set for command execution.
     */
    public function __construct(Controller $context)
    {
        $this->context = $context;
    }

    /**
     * Get the context of the command execution.
     *
     * @return Controller Current execution context.
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Get corresponding view to display.
     * @return View
     */
    abstract protected function getView();

    /**
     * Method, containing the main execution algorithm.
     * @abstract
     */
    abstract protected function doExecute();

    /**
     * Things to do on exception caught.
     * Setting fallback view, perform necessary cleanup.
     * @param \Exception $ex Caught exception.
     * @return void
     */
    abstract protected function onException(\Exception $ex);

}
