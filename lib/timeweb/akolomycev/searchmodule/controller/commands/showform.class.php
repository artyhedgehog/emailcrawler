<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 26.06.14
 * Time: 20:13
 */

namespace Timeweb\AKolomycev\SearchModule\controller\commands;

use Timeweb\AKolomycev\SearchModule\view\ErrorPage;
use Timeweb\AKolomycev\SearchModule\view\FormPage;
use Timeweb\AKolomycev\SearchModule\view\View;

/**
 * Command shows search form.
 * @package Timeweb\AKolomycev\SearchModule\controller\commands
 */
class ShowForm extends Command
{
    /**
     * @var View Used view.
     */
    private $view;

    /**
     * Method, containing the main execution algorithm.
     */
    protected function doExecute()
    {
        $this->view = new FormPage($this->getContext());
    }

    /**
     * Get corresponding view to display.
     * @return View
     */
    protected function getView()
    {
        return $this->view;
    }

    /**
     * Things to do on exception caught.
     * Setting fallback view, perform necessary cleanup.
     * @param \Exception $ex Caught exception.
     * @return void
     */
    protected function onException(\Exception $ex)
    {
        $this->view = new ErrorPage($this->getContext());
        $this->view->setValue('error', array('msg' => $ex->getMessage(),
                                             'trc' => $ex->getTraceAsString()));
    }
}
