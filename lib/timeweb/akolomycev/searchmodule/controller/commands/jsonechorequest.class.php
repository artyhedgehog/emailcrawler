<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 04.07.14
 * Time: 17:25
 */

namespace Timeweb\AKolomycev\searchmodule\controller\commands;


use Timeweb\AKolomycev\searchmodule\view\JsonView;
use Timeweb\AKolomycev\SearchModule\view\View;

/**
 * Echoes request in JSON format.
 * Debugging purpose command.
 * @package Timeweb\AKolomycev\searchmodule\controller\commands
 */
class JsonEchoRequest extends Command
{
    protected function doExecute()
    {}

    /**
     * Get corresponding view to display.
     * @return View
     */
    protected function getView()
    {
        $view = new JsonView($this->getContext());
        $view->setValues($_REQUEST);
        return $view;
    }
}