<?php
namespace Timeweb\AKolomycev\SearchModule\controller;

/**
 * Request representation.
 *
 * @author a.kolomycev
 */
class Request
{
    /**
     * @var string Request property name affecting command to load.
     */
    const COMMAND_PROPERTY_NAME = 'cmd';

    /**
     * @var array Properties received via HTTP request.
     */
    protected $properties;

    /**
     * Constructor method.
     *
     * Saves HTTP request as properties or calls CLI args parser.
     */
    public function __construct()
    {
        $this->properties = $_REQUEST;
    }

    /**
     * Getting request property value.
     *
     * @param string $name Name of the property.
     * @return mixed Returns the property value.
     */
    public function getProperty($name)
    {
        if (array_key_exists($name, $this->properties)) {
            return $this->properties[$name];
        } else {
            return null;
        }
    }

}
