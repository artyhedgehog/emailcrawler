<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 26.06.14
 * Time: 18:16
 */

namespace Timeweb\AKolomycev\SearchModule\controller;

use Timeweb\AKolomycev\SearchModule\controller\commands\Command;
use Timeweb\AKolomycev\searchmodule\controller\commands\JsonEchoRequest;
use Timeweb\AKolomycev\SearchModule\controller\commands\ParsePage;
use Timeweb\AKolomycev\SearchModule\controller\commands\ShowForm;
use Timeweb\AKolomycev\SearchModule\controller\commands\ShowResults;

/**
 * Controller front controller.
 *
 * @package Timeweb\AKolomycev\SearchModule\controllers
 */
class Controller
{
    const COMMAND_SHOW_FORM = 'form';
    const COMMAND_SHOW_RESULTS = 'results';
    const COMMAND_PARSE_PAGE = 'search';

    /**
     * @var Request Request object.
     */
    protected $request;

    /**
     * Run application method.
     * @return void
     */
    public static function run()
    {
        $app = new self(new Request());
        $cmd = $app->getCommand();
        $view = $cmd->execute();
        $view->display();
    }

    /**
     * @param Request $request Request object used by application
     */
    protected final function __construct(Request $request)
    {
        $this->request = $request;
        $this->command = $this->getCommand();
    }

    /**
     * @return Command A command defined in a request.
     */
    protected function getCommand()
    {
        switch ($this->request->getProperty(Request::COMMAND_PROPERTY_NAME)) {
            default:
            case null:
            case self::COMMAND_SHOW_FORM:
                return new ShowForm($this);
            case self::COMMAND_PARSE_PAGE:
//                return new JsonEchoRequest($this);
                return new ParsePage($this);
            case self::COMMAND_SHOW_RESULTS:
                return new ShowResults($this);
        }
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }
}