<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 02.07.14
 * Time: 10:04
 */

namespace Timeweb\AKolomycev\utils;

/**
 * Contains functions to validate different parameters.
 * @package Timeweb\AKolomycev\utils
 */
class Validator
{
    /**
     * Throws an exception if given variable is of a wrong type.
     *
     * @param mixed $var Variable to check
     * @param string $type Expected type name.
     * @param string $argName Argument name.
     * @param string $functionName Invoked function or method name.
     * @return void
     * @throws \InvalidArgumentException
     */
    public static function validateArgType($var, $type, $argName = null,
                                           $functionName = null
    ) {
        $varType = gettype($var);
        if ($varType !== $type) {
            $place = '';
            if (isset($argName)) {
                $place .= " for \${$argName} ";
                if (isset($functionName)) {
                    $place .= "in {$functionName}()";
                }
            }
            $msg = "Given argument of a type '{$varType}'{$place} - {$type} expected.";
            throw new \InvalidArgumentException($msg);
        }
    }

}