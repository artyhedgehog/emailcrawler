<?php
/**
 * Created by PhpStorm.
 * User: a.kolomycev
 * Date: 26.06.14
 * Time: 17:18
 */

namespace Timeweb\AKolomycev\utils;

/**
 * Autoloader of classes.
 *
 * Searches for files in namespace-like directory hierarchy. Uses standard
 * spl_autoload implementation. Requires lowercase names.
 *
 * @package Timeweb\AKolomycev\SearchModule\base
 */
class ClassAutoloader
{
    /**
     * @var string Full path for library directory.
     */
    private $libFullPath;

    public function __construct()
    {
        $namespacePath = str_replace('\\', DIRECTORY_SEPARATOR, __NAMESPACE__);
        // RegExp pattern with path separator as delimiter, ignorecased.
        $pattern = PATH_SEPARATOR . $namespacePath . '$' . PATH_SEPARATOR . 'i';
        $this->libFullPath = preg_replace($pattern, '', __DIR__);
    }

    /**
     * @return void
     */
    public function register()
    {
        set_include_path(get_include_path() . PATH_SEPARATOR
                         . $this->libFullPath);
        spl_autoload_extensions('.class.php');
        spl_autoload_register();
    }
}