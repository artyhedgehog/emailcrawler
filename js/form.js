$(document).ready(function() {
	'use strict';

	var searchForm = $('#search_form');

	var urlPattern = new RegExp('^(https?://)?' +
								'([-a-zа-я0-9]+(?:\\.[-a-zа-я0-9]+)+)' +
								'((?:/(?:[-\\w\\.!~*\'\\(\\)]|%\\w{2})*)*)' +
								'((?:\\?(?:[-\\w\\.!~*\'\\(\\)]|%\\w{2})+(?:=(?:[-\\w\\.!~*\'\\(\\)]|%\\w{2})+)?(?:&(?:[-\\w\\.!~*\'\\(\\)]|%\\w{2})+(?:=(?:[-\\w\\.!~*\'\\(\\)]|%\\w{2})+)?)*)?)',
                                'i');

	var urlBlock = $('#url_block');
	var urlInput = $('#url_input');
	var urlHint = $('#url_hint');

	/**
	 * Checked and escaped value of page URL
	 */
	var validUrl;

	var typeRadios = $('input[type=radio]');

	var searchTextBlock = $('#search_text_block');
	var searchTextInput = $('#search_text_input');
	var searchTextHint = $('#search_text_hint');

	var submitHint = $('#submit_hint');

	/**
	 * Defines names and description of fields validation errors.
	 * @type {{noUrl: string, wrongUrl: string, noText: string, 0: string}}
	 */
	var errors = {
		noUrl: 'You need to provide a site URL.',
		wrongUrl: 'Illegal URL.',
		noText: 'You need to fill in some text to search.',
		0: ''
	};

	/**
	 * Defines onchange listener for search type radio inputs.
	 * Shows a text-to-search field if type set to text, hides otherwise.
	 */
	function onTypeRadioChanged() {
		if ($(this).filter(':checked').val() === 'text') {
			searchTextBlock.show();
		} else {
		    searchTextBlock.hide();
		}
	}

	typeRadios.change(onTypeRadioChanged);

	/**
     * Validate URL.
	 * Checks URL field for errors and sets corrected and escaped value to a
	 * url variable.
	 * @returns {number|string} Name of error found, 0 if none.
	 */
	function validateUrl() {
		// If validUrl already set, report success.
		if (validUrl !== undefined) {
			return 0;
		}

		var error = 0;
		var url = urlInput.val();
		var matches;

		if (url.length === 0) {
			error = 'noUrl';
		} else {
			matches = url.match(urlPattern);
			if (matches === null) {
				error = 'wrongUrl';
			} else {
				// Add protocol if none specified.
				if (!matches[1]) {
					matches[1] = 'http://';
				}
				matches[2] = punycode.toASCII(matches[2]);
				matches.shift(); // Remove whole-entry part.
				validUrl = matches.join('');
			}
		}

		urlHint.text(errors[error]);
		if (error) {
			urlBlock.addClass('error');
		} else {
			urlBlock.removeClass('error');
		}
		return error;
	}

	/**
	 * Set blur listener for URL field.
	 */
	urlInput.blur(validateUrl);

	urlInput.change(function () {
		// If URL field changes, clear validUrl.
		validUrl = undefined;
	});

	/**
	 * Check text field for errors.
	 * @returns {number|string} Name of error found, 0 if none.
	 */
	function validateSearchText() {
		var error = 0;
		if (typeRadios.filter(':checked').val() === 'text' &&
				searchTextInput.val().length === 0) {
			error = 'noText';
		}
		searchTextHint.text(errors[error]);
		if (error) {
			searchTextBlock.addClass('error');
		} else {
			searchTextBlock.removeClass('error');
		}
		return error;
	}

	/**
	 * Set blur listener for search text field.
	 */
	searchTextInput.blur(validateSearchText);

	//noinspection JSUnusedLocalSymbols
	/**
	 * Submit success listener.
	 * Checks answer from server and puts a message in hint field.
	 * @param data
	 * @param textStatus
	 */
	function onSubmitSuccess(data, textStatus) {
		switch (data.res) {
			case 'ok':
				submitHint.text('Page has been successfully parsed.');
				searchForm.removeClass('error');
				break;
			case 'error':
				submitHint.text('Failed to parse page. ' + data.msg);
				searchForm.addClass('error');
				// Log stack trace:
				console.log(data.trc);
				break;
			default:
				submitHint.text('Unknown error occured.');
				console.error(data);
				searchForm.addClass('error');
		}
	}

	//noinspection JSUnusedLocalSymbols
	/**
	 * Submit failure listener.
	 * @param jqXHR
	 * @param textStatus
	 * @param errorThrown
	 */
	function onSubmitError(jqXHR, textStatus, errorThrown) {
		submitHint.text('Request failed with message: ' + textStatus + '.');
		console.log(errorThrown);
	}

	/**
	 * Function to call on submitting form.
	 * @param form
	 */
	function onSubmitForm(form) {
		form.preventDefault();
		if (validateSearchText() || validateUrl()) {
			submitHint.text(
					'You need to correct errors in the form before submitting.');
			searchForm.addClass('error');
		} else {
			submitHint.text('');
			var data = {};
			data[urlInput.attr('name')] = validUrl;
			data[typeRadios.attr('name')] = typeRadios.filter(':checked').val();
			if (data[typeRadios.attr('name')] === 'text') {
				data[searchTextInput.attr('name')] = searchTextInput.val();
			}

			$.ajax({
				url: searchForm.attr('action'),
				type: searchForm.attr('method'),
				data: data,
				dataType: 'json',
				success: onSubmitSuccess,
				error: onSubmitError
			});
		}
	}

	searchForm.submit(onSubmitForm);

});
