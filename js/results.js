$(document).ready(function() {
	'use strict';
	var expanders = $('a.expander');
	var contentsContainers = $('.contents');
	expanders.click(function() {
		var page = $(this).closest('div.page');
		contentsContainers.hide();
		page.children('.contents').show();
	});
});
